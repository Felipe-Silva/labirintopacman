
let map = [
    "WWWWWWWWWWWWWWWWWWWWW",
    "W   W     W     W W W",
    "W W W WWW WWWWW W W W",
    "W W W   W     W W   W",
    "W WWWWWWW W WWW W W W",
    "W         W     W W W",
    "W WWW WWWWW WWWWW W W",
    "W W   W   W W     W W",
    "W WWWWW W W W WWW W F",
    "S     W W W W W W WWW",
    "WWWWW W W W W W W W W",
    "W     W W W   W W W W",
    "W WWWWWWW WWWWW W W W",
    "W       W       W   W",
    "WWWWWWWWWWWWWWWWWWWWW",
];


let terreno = document.getElementById('terreno');

const criarMapa = (array) => {
    let muro;
    let bloco;
    let movimento;
    let posicaoInicial;
    let posicaoFinal;

    for(let i = 0; i < array.length; i++){
        muro = document.createElement('div');
        muro.classList.add('muro');
        for(let j = 0; j < array[i].length; j++){
            if(array[i][j] === "W"){
            bloco = document.createElement('div');
            bloco.classList.add('caminho');
            bloco.classList.add('bloco');
            muro.appendChild(bloco);  
            } else if(array[i][j] === "S"){
            posicaoInicial = document.createElement('div');
            posicaoInicial.classList.add('caminho')
            posicaoInicial.classList.add('inicio');
            muro.appendChild(posicaoInicial);
            } else if(array[i][j] === "F"){
            posicaoFinal = document.createElement('div');
            posicaoFinal.classList.add('caminho')
            posicaoFinal.classList.add('final');
            muro.appendChild(posicaoFinal);
            } else{
            movimento = document.createElement('div');
            movimento.classList.add('caminho')
            movimento.classList.add('movimento');
            muro.appendChild(movimento);
            }
        }
        terreno.appendChild(muro);
    }
};

criarMapa(map);

function caminhoInicial(arr){
    let posicaoInicialJogador;
    let posicao = 0;

    for(let i = 0; i < arr.length; i++){
        for(let j = 0; j < arr[i].length; j++){
            posicao++;

            if(arr[i][j] == "S"){
                posicaoInicialJogador = posicao;
                return posicaoInicialJogador;
            }
        }
    }

}

function caminhoFinal(arr){
    let posicaoFinalJogador;
    let  posicao = 0;

    for(let i = 0; i < arr.length; i++){
        for(let j = 0; j < arr[i].length; j++){
            posicao++;

            if(arr[i][j] == "F"){
                posicaoFinalJogador =  posicao;
                return posicaoFinalJogador;
            }
        }
    }
}

function posicaoJogador(position){
    let movimentoNoCaminho = document.querySelectorAll('.caminho')[position-1];

    let pacMan = document.createElement('div');
    pacMan.setAttribute('id','jogador');
    
    movimentoNoCaminho.appendChild(pacMan);
}

posicaoJogador(caminhoInicial(map));

let posicaoAtual = caminhoInicial(map)-1;
let ultimaPosicao =  posicaoAtual;

document.addEventListener('keydown', function(event){
    let inicio = document.querySelectorAll('.caminho')[ultimaPosicao].lastElementChild;
    let proximaPosicao;
    const keyName = event.key;


    if(keyName === "ArrowDown"){
        proximaPosicao = ultimaPosicao;
        proximaPosicao += 21;
    } else if(keyName === "ArrowUp"){
        proximaPosicao = ultimaPosicao;
        proximaPosicao -= 21;
    } else if(keyName === "ArrowLeft"){
        proximaPosicao = ultimaPosicao;
        proximaPosicao -= 1;
    } else if(keyName === "ArrowRight"){
        proximaPosicao = ultimaPosicao;
        proximaPosicao += 1;
    }

    if(!movimentoInvalido(proximaPosicao)){
        let final = document.querySelectorAll('.caminho')[proximaPosicao];
        final.appendChild(inicio);
        posicaoAtual = proximaPosicao
    } else{
        return;
    }
    ultimaPosicao = posicaoAtual;

    vencedor(map);
});

function vencedor(arr){
    let saida = document.querySelectorAll('.caminho')[caminhoFinal(arr)-1].lastElementChild.id;
    let div = document.getElementById("container")
    let span = document.createElement("span")
    

    
    if(saida === 'jogador'){
        span.textContent = 'You won!!!'
        document.getElementById('msgVitoris').style.display = 'block'
        div.appendChild(span)
        map = [
            "WWWWWWWWWWWWWWWWWWWWW",
            "W   W     W     W W W",
            "W W W WWW WWWWW W W W",
            "W W W   W     W W   W",
            "W WWWWWWW W WWW W W W",
            "W         W     W W W",
            "W WWW WWWWW WWWWW W W",
            "W W   W   W W     W W",
            "W WWWWW W W W WWW W W",
            "W     W W W W W W WWW",
            "WWWWW W W W W W W W W",
            "W     W W W   W W W W",
            "W WWWWWWW WWWWW W W W",
            "W       W       W   W",
            "WWWWWWWWWWWWWWWWWWWWW",
        ];

        return true;
    }
}

function movimentoInvalido(position){
    let invalidMovement = document.querySelectorAll('.caminho')[position].classList.contains('bloco')

    return invalidMovement;
}

const buttonReset = document.getElementById('reset_button');

function resetButton (){
    buttonReset.addEventListener('click', () => location.reload())
}
resetButton();